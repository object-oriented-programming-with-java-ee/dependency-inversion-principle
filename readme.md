# Dependency inversion principle

## Defining the principle

In order to adhere to this principle we need to adhere to two requirements:

1. High-level modules should not depend on low-level modules. Both should depend on abstractions (e.g. interfaces).
1. Abstractions should not depend on details. Details (concrete implementations) should depend on abstractions.

## Adhering to, and violating the principle

In this project the PolicyImpl class violates the dependency inversion principle.  It depends on the concrete MechanismImpl class.

The MechanismImpl class adheres to it.  

## Remember Liskov substitution

We must take care to adhere to the Liskov substitution principal when considering the DI principle.

The classes implementing the abstractions must be semantic subtypes - they must behave the same way.  Remember that it is possible for two classes to have the same parent but not be subtypes.  