package mechanism;

import mechanismservices.MechanismService;
import utilityservices.UtilityService;

public class MechanismImpl implements MechanismService {

	UtilityService utility;
	
	// we depend on a (hopefully stable) abstraction and adhere to the 
	// dependency inversion principle
	public MechanismImpl(UtilityService utilityService)
	{
		this.utility = utilityService;
	}
	
}
