package policy;

import policyservices.PolicyService;

// using a concrete class in your "import" statements is a sign that you are
// violating the dependency inversion principle 
import mechanism.MechanismImpl;

public class PolicyImpl implements PolicyService {

	private MechanismImpl mechanism;
	
	// this violates the inversion of dependency rule because we are depending
	// on a concrete class.  Compare it to the constructor of MechanismImpl
	public PolicyImpl(MechanismImpl mechanism)
	{
		this.mechanism = mechanism;
	}
	
}
